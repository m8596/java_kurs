package com.lscc.gol;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class GridTest {
	@Test
	public void evaluateGliderGrid() throws Exception {
		Grid grid = aGrid(10);
		assertEquals(grid.size(), 10);
        grid.put(LiveCell.INSTANCE, 4, 6);
        grid.put(LiveCell.INSTANCE, 5, 6);
        grid.put(LiveCell.INSTANCE, 6, 6);
        grid.put(LiveCell.INSTANCE, 6, 5);
        grid.put(LiveCell.INSTANCE, 5, 4);
        assertEquals(grid.liveCellsSize(), 5);
        System.out.println("t0:");
		System.out.println(grid);
		
		
		for(int iterationen = 1; iterationen < 5; iterationen = iterationen + 1) {
			grid = grid.nextGeneration();
			System.out.print("t");
			System.out.print(iterationen);
			System.out.print(":");
			System.out.println(grid);
			switch(iterationen) {
			case 1:
		        Grid t1Grid = aGrid(10);
		        t1Grid.put(LiveCell.INSTANCE, 5, 7);
		        t1Grid.put(LiveCell.INSTANCE, 5, 6);
		        t1Grid.put(LiveCell.INSTANCE, 6, 6);
		        t1Grid.put(LiveCell.INSTANCE, 4, 5);
		        t1Grid.put(LiveCell.INSTANCE, 6, 5);
		        assertEquals(grid, t1Grid);
		        break;
			case 2:
		        Grid t2Grid = aGrid(10);
		        t2Grid.put(LiveCell.INSTANCE, 5, 7);
		        t2Grid.put(LiveCell.INSTANCE, 6, 7);
		        t2Grid.put(LiveCell.INSTANCE, 4, 6);
		        t2Grid.put(LiveCell.INSTANCE, 6, 6);
		        t2Grid.put(LiveCell.INSTANCE, 6, 5);
		        assertEquals(grid, t2Grid);
		        break;
		        
			case 3:
		        Grid t3Grid = aGrid(10);
		        t3Grid.put(LiveCell.INSTANCE, 5, 7);
		        t3Grid.put(LiveCell.INSTANCE, 6, 7);
		        t3Grid.put(LiveCell.INSTANCE, 6, 6);
		        t3Grid.put(LiveCell.INSTANCE, 7, 6);
		        t3Grid.put(LiveCell.INSTANCE, 5, 5);
		        assertEquals(t3Grid, grid);
		        break;
		        
			case 4:
		        Grid t4Grid = aGrid(10);
		        t4Grid.put(LiveCell.INSTANCE, 5, 7);
		        t4Grid.put(LiveCell.INSTANCE, 6, 7);
		        t4Grid.put(LiveCell.INSTANCE, 7, 7);
		        t4Grid.put(LiveCell.INSTANCE, 7, 6);
		        t4Grid.put(LiveCell.INSTANCE, 6, 5);
		        assertEquals(t4Grid, grid);
		        break;
				
				
				
			}
		}
		
	}
	
    @Test
    public void initializeGridWithSize() throws Exception {
        Grid grid = aGrid(10);
        assertEquals(grid.size(), 10);

    }

    @Test
    public void populateGridWithLiveAndDeadCells() throws Exception {
        Grid grid = aGrid(10);
        grid.put(LiveCell.INSTANCE, 0, 0);
        grid.put(LiveCell.INSTANCE, 0, 1);
        grid.put(LiveCell.INSTANCE, 0, 2);
        grid.put(DeadCell.INSTANCE, 0, 3);
        assertEquals(grid.liveCellsSize(), 3);
    }

    @Test
    public void evaluateGridWithDeadCells() throws Exception {
        Grid grid = aGrid(3);
        Grid newGrid = grid.nextGeneration();
        Grid expectedGrid = aGrid(3);
        assertEquals(newGrid, expectedGrid);
    }

    @Test
    public void evaluateGridWithOneLiveCells() throws Exception {
        Grid grid = aGrid(3);
        grid.put(LiveCell.INSTANCE, 0, 0);
        Grid newGrid = grid.nextGeneration();
        Grid expectedGrid = aGrid(3);
        assertEquals(expectedGrid, newGrid);
    }

    @Test
    public void evaluateBlinkerGrid() throws Exception {
        Grid grid = aGrid(10);
        grid.put(LiveCell.INSTANCE, 4, 3);
        grid.put(LiveCell.INSTANCE, 4, 4);
        grid.put(LiveCell.INSTANCE, 4, 5);

        Grid newGrid = grid.nextGeneration();
        Grid expectedGrid = aGrid(10);
        expectedGrid.put(LiveCell.INSTANCE, 3, 4);
        expectedGrid.put(LiveCell.INSTANCE, 4, 4);
        expectedGrid.put(LiveCell.INSTANCE, 5, 4);
        assertEquals(expectedGrid, newGrid);

        Grid startingPosition = newGrid.nextGeneration();
        assertEquals(grid, startingPosition);

        System.out.println(grid);
        System.out.println(expectedGrid);
        System.out.println(startingPosition);
    }

    private Grid aGrid(int size) {
        return new Grid(size);
    }
}
